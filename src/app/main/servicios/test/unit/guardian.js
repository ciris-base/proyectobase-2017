"use strict";

var test = require( "tape" );
var Factory = require( "../../guardian.js" );

var permisosAdmin = {
  modulo: {
    submodulo: {
      ver: true,
      editar: true,
      crear: true,
      eliminar: true
    }
  }
};

var permisosRegular = {
  modulo: {
    submodulo: {
      ver: false,
      editar: false,
      crear: false,
      eliminar: false
    }
  }
};

test( "GUARDIAN: Dejar crear si tiene permiso", dejarPasarCrear );
test( "GUARDIAN: Dejar editar si tiene permiso", dejarPasarEditar );
test( "GUARDIAN: Dejar editar si tiene permiso", dejarPasarVer );
test( "GUARDIAN: NO dejar crear si NO tiene permiso", noDejarPasarCrear );
test( "GUARDIAN: NO dejar editar si NO tiene permiso", noDejarPasarEditar );
test( "GUARDIAN: NO dejar editar si NO tiene permiso", noDejarPasarVer );
test( "GUARDIAN: Abrir - Devolver false si no existe el modulo", revisarExistenciaModulo1 );
test( "GUARDIAN: Filtrar los registros que puede ver", filtrarRegistros );

function dejarPasarCrear( assert ) {
  var guardian = instanciar( permisosAdmin );
  var resul = guardian.dejarAbrir( {}, "modulo", "submodulo" );
  assert.deepEqual( resul, true );
  assert.end();
}

function dejarPasarEditar( assert ) {
  var guardian = instanciar( permisosAdmin );
  var params = {id: "123", editar: "true"};
  var resul = guardian.dejarAbrir( params, "modulo", "submodulo" );
  assert.deepEqual( resul, true );
  assert.end();
}

function dejarPasarVer( assert ) {
  var guardian = instanciar( permisosAdmin );
  var params = {id: "123", editar: "false"};
  var resul = guardian.dejarAbrir( params, "modulo", "submodulo" );
  assert.deepEqual( resul, true );
  assert.end();
}

function noDejarPasarCrear( assert ) {
  var guardian = instanciar( permisosRegular );
  var resul = guardian.dejarAbrir( {}, "modulo", "submodulo" );
  assert.deepEqual( resul, false );
  assert.end();
}

function noDejarPasarEditar( assert ) {
  var guardian = instanciar( permisosRegular );
  var params = {id: "123", editar: "true"};
  var resul = guardian.dejarAbrir( params, "modulo", "submodulo" );
  assert.deepEqual( resul, false );
  assert.end();
}

function noDejarPasarVer( assert ) {
  var guardian = instanciar( permisosRegular );
  var params = {id: "123", editar: "false"};
  var resul = guardian.dejarAbrir( params, "modulo", "submodulo" );
  assert.deepEqual( resul, false );
  assert.end();
}

function revisarExistenciaModulo1( assert ) {
  var guardian = instanciar( permisosRegular );
  var resul = guardian.dejarAbrir( {}, "modulo", "otro" );
  assert.deepEqual( resul, false );
  assert.end();
}

function filtrarRegistros( assert ) {
  var permisos = {
    operaciones: {
      zanahorias: {
        entrada: {ver: true},
        seleccion: {ver: false},
        entradaCamara: {ver: false},
        empaque: {ver: false}
      }
    }
  };
  var guardian = instanciar( permisos );
  var registros = [
    {paso: "entrada"},
    {paso: "entrada"},
    {paso: "entradaCamara"},
    {paso: "empaque"},
    {paso: "seleccion"}
  ];
  var filtro = guardian.filtrarRegistros( "operaciones", "zanahorias", registros );
  assert.deepEqual( filtro.length, 2 );
  assert.deepEqual( filtro[0].paso, "entrada" );
  assert.deepEqual( filtro[1].paso, "entrada" );
  assert.end();
}

function instanciar( permisos ) {
  var auth = {
    getPayload: function() {
      this.permisos = permisos;
      return this;
    }
  };
  var factory = new Factory();
  factory.setPermisos( auth );
  return factory;
}
