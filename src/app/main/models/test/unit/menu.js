"use strict";

var test = require( "tape" );
var _ = require( "lodash" );
var menu = require( "../../menu.js" );

test( "Agrupar hijos - los hijos de un estado deben quedar DENTRO del padre", function ( assert ) {
  var prueba = [
    {name: "index.doc"},
    {name: "index.otro"},
    {name: "index.doc.hijo1"},
    {name: "index.doc.hijo2"}
  ];
  var res = [
    {name: "index.doc", hijos: [ {name: "index.doc.hijo1"}, {name: "index.doc.hijo2"} ]},
    {name: "index.otro", hijos:[]}
  ];
  assert.deepEqual( menu.agruparHijos( prueba ), res );
  assert.end();
} );

test( "Agrupar hijos - sin hijos", function ( assert ) {
  var prueba = [
    {name: "index.doc"},
    {name: "index.otro"},
    {name: "index.prueba.hijo1"},
    {name: "index.mabel.hijo2"}
  ];
  var res = [
    {name: "index.doc",  hijos:[]},
    {name: "index.otro",  hijos:[]},
    {name: "index.prueba.hijo1",  hijos:[]},
    {name: "index.mabel.hijo2",  hijos:[]}
  ];
  assert.deepEqual( menu.agruparHijos( prueba ), res );
  assert.end();
} );


test( "Rechazar estados abstractos", function( assert ) {
  var prueba = [ {name:"No abstracto"}, {name: "Abstracto", abstract: true} ];
  assert.equal( menu.rejectAbstractos( prueba )[0], prueba[0] );
  assert.end();
} );

test( "Rechazar estados abstractos - empty", function( assert ) {
  var prueba = [ ];
  assert.equal( _.isEmpty( menu.rejectAbstractos( prueba ) ),  true );
  assert.end();
} );

test( "Rechazar estados abstractos - null", function( assert ) {
  assert.equal( _.isEmpty( menu.rejectAbstractos( null ) ),  true );
  assert.end();
} );

test( "Rechazar estados abstractos - sin abstractos", function( assert ) {
  var prueba = [ {name:"No abstracto"}, {name: "Abstracto"} ];
  assert.equal( _.size( menu.rejectAbstractos( prueba ) ),  2 );
  assert.end();
} );

test( "Rechazar estados ocultos", function( assert ) {
  var prueba = [ {name:"No abstracto"}, {name: "Abstracto", data: {oculto: true}} ];
  assert.equal( menu.rejectOcultos( prueba )[0], prueba[0] );
  assert.end();
} );

test( "Rechazar estados ocultos - empty", function( assert ) {
  var prueba = [ ];
  assert.equal( _.isEmpty( menu.rejectOcultos( prueba ) ),  true );
  assert.end();
} );

test( "Rechazar estados ocultos - null", function( assert ) {
  assert.equal( _.isEmpty( menu.rejectOcultos( null ) ),  true );
  assert.end();
} );

test( "Rechazar estados ocultos - sin ocultos", function( assert ) {
  var prueba = [ {name:"No abstracto"}, {name: "Abstracto"} ];
  assert.equal( _.size( menu.rejectOcultos( prueba ) ),  2 );
  assert.end();
} );
