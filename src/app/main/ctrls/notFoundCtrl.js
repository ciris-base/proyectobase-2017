"use strict";

module.exports = NotFoundCtrl;

function NotFoundCtrl( $state ) {
  var nfc = this;
  nfc.volver = volver;
  nfc.volverMain = volverMain;

  function volver() {
    window.history.back();
  }

  function volverMain() {
    $state.go( "index.main" );
  }
}
