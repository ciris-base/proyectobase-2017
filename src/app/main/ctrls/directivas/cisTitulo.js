"use strict";

module.exports = cisTitulo;

function cisTitulo( $rootScope, $timeout ) {
  return {
    restrict: "EA",
    scope: true,
    link: link
  };

  function link( scope, element ) {
    $rootScope.$on( "$stateChangeStart", change );

    function change( event, toState ) {
      $timeout( function() {
        element.text(  toState.data.titulo );
      } );
    }
  }

}
