"use strict";

module.exports = selector;
var _ = require( "lodash" );

function selector( $http, backend ) {
  return {
    restrict: "EA",
    require: "?ngModel",
    replace: true,
    scope: {
      id: "@",
      name: "@",
      ruta: "@",
      atributo: "@",
      modelo: "=?",
      ngDisabled: "=?",
      ngRequired: "=?",
      listaSimple: "=?"
    },
    templateUrl: "main/ctrls/directivas/selector/selector.html",
    link: link
  };

  function link( scope, element, attrs, ctrl ) {
    scope.setearModelo = setearModelo;
    buscar().then( function( data ) {
      scope.lista = data;
      if ( ctrl.$viewValue ) {
        var obj = _.find( scope.lista, {"_id": ( typeof ctrl.$viewValue === "object" ? ctrl.$viewValue._id : ctrl.$viewValue )} );
        scope.opcion = obj._id;
        ctrl.$setViewValue( obj );
      }
    } );

    function buscar() {
      scope.texto = "--Cargando--";
      return $http.get( backend + scope.ruta, {ignoreLoadingBar: true} ).then( setLista );

      function setLista( resp ) {
        var data = _.get( resp, "data.docs" ) || resp.data;
        if ( data.length === 0 ) {
          scope.texto = "--No hay registros--";
          return [];
        } else {
          scope.texto = "--Seleccione--";
          if ( scope.modelo ) {
            return _.map( data, function( elem ) {
              return scope.modelo( elem );
            } );
          } else {
            if ( scope.listaSimple ) {
              return _.map( data, function( elem ) {
                return elem[scope.atributo];
              } );
            }
            return data;
          }
        }
      }
    }

    function setearModelo() {
      if ( scope.listaSimple ) {
        ctrl.$setViewValue( scope.opcion );
      } else {
        var filtrado = _.filter( scope.lista, {"_id": scope.opcion} );
        ctrl.$setViewValue( _.first( filtrado ) );
      }
    }
  }
}
