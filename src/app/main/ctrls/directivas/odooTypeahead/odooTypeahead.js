//Basado en: https://github.com/raymondmuller/ng-typeahead

"use strict";

module.exports = odooTypeahead;

var _ = require( "lodash" );
var odoo = require( "./jsonRcpOdoo.js" )();

function odooTypeahead( $log, $timeout, odooHost ) {
  return {
    restrict: "E",
    scope: {
      data: "=?",
      delay: "=?",
      forceSelection: "=?",
      limit: "=?",
      startFilter: "=?",
      threshold: "=?",
      onBlur: "=?",
      onSelect: "=?",
      onType: "=?",
      ngDisabled: "=?",
      ngRequired: "=?",
      supplier: "=?",
      name: "@"
    },
    require: "?ngModel",
    transclude: true,
    link: link,
    templateUrl: "main/ctrls/directivas/odooTypeahead/template.html"
  };

  function link( scope, elem, attrs, ngModel ) {
    var selectedItem = null;
    var KEY = {
      UP: 38,
      DOWN: 40,
      ENTER: 13,
      TAB: 9,
      ESC: 27
    };
    var selectedLabel = scope.search;
    var itemSelected = false;
    scope.index = 0;
    if ( !scope.delay ) {
      scope.delay = 0;
    }
    scope.placeholder = attrs.placeholder;
    if ( _.isUndefined( scope.limit ) ) {
      scope.limit = Infinity;
    }
    if ( _.isUndefined( scope.threshold ) ) {
      scope.threshold = 0;
    }
    if ( _.isUndefined( scope.forceSelection ) ) {
      scope.forceSelection = false;
    }

    $timeout( function() {
      var v = ngModel.$viewValue;
      if ( ngModel.$viewValue ) {
        selectedItem = v;
        selectedLabel = v.name;
        scope.search = v.name;
      }
    }, 100 );

    scope.$watch( "search", function( v ) {
      scope.index = 0;
      return jsonRcpOdoo( v ).then( function( data ) {
        scope.data = data;
        itemSelected = false;
        if ( v === selectedLabel || _.isUndefined( v ) ) {
          scope.showSuggestions = false;
          scope.noResults = false;
        } else {
          scope.showSuggestions = scope.data.length && scope.search && scope.search.length > scope.threshold;
          scope.noResults = !scope.data.length && scope.search && scope.search.length > scope.threshold;
        }
        return ngModel.$setViewValue( selectedItem || v );
      } );
    } );
    scope.$onBlur = $onBlur;
    scope.$onSelect = $onSelect;
    scope.$onKeyDown = $onKeyDown;

    function $onKeyDown( event ) {
      switch ( event.keyCode ) {
        case KEY.UP:
          if ( scope.index > 0 ) {
            scope.index -= 1;
          } else {
            scope.index = scope.data.length - 1;
          }
          break;
        case KEY.DOWN:
          if ( scope.index < ( scope.data.length - 1 ) ) {
            scope.index += 1;
          } else {
            scope.index = 0;
          }
          break;
        case KEY.ENTER:
          return scope.$onSelect( scope.data[scope.index] );
        case KEY.TAB:
          return scope.$onSelect( scope.data[scope.index] );
        case KEY.ESC:
          scope.showSuggestions = false;
          return;
        default:
      }
    }

    function jsonRcpOdoo( val ) {
      var query = {
        model: attrs.odooModel,
        fields: [ "name", "ref", "bank_ids", "property_account_payable" ],
        domain: [ [ "name", "ilike", val ] ],
        context: {},
        limit: 10,
        sort: "name"
      };
      if ( scope.supplier ) {
        query.domain.push( [ "supplier", "=", true ] );
      }
      return odoo.rpc( "http://" + odooHost + "/web/dataset/search_read", query, {} )
      .then( function( results ) {
        return results.records;
      } );
    }

    function $onBlur() {
      if ( !itemSelected && scope.forceSelection ) {
        scope.search = selectedLabel;
      }
      scope.showSuggestions = false;
      return;
    }

    function $onSelect( item ) {
      selectedLabel = item.name;
      selectedItem = item;
      scope.search = item.name;
      itemSelected = true;
      if ( scope.onSelect ) {
        scope.onSelect( item );
      }
      scope.showSuggestions = false;
      return $timeout( function() {
        return;
      } );
    }
  }
}
