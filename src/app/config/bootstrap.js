"use strict";

var angular = require( "angular" );
module.exports = bootstrap;

function bootstrap( modulo ) {
  angular.element( document ).ready( function() {
    var yaInstanciado = angular.element( document ).injector();
    if ( !yaInstanciado ) {
        angular.bootstrap( document, [ modulo ] );
    }
  } );
}
