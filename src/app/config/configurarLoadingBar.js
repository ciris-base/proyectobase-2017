"use strict";

module.exports = configurarLoadingBar;

function configurarLoadingBar( cfpLoadingBarProvider ) {
  var bar = "<div class='loading-bar'><div class='loading-bar__bar'>" +
  "<div class='loading-bar__peg'></div></div></div>";
  //var spinner = "<div class='loading-bar__spinner'><i class='fa fa-spinner fa-spin fa-fw'></i></div>";
  cfpLoadingBarProvider.loadingBarTemplate = bar;
  //cfpLoadingBarProvider.spinnerTemplate = spinner;

}
