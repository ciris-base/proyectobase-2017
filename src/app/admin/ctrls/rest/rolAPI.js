"use strict";

module.exports = RolAPI;
var _ = require( "lodash" );

var rol = require( "../../models/rol.js" );

function RolAPI( backend, $http ) {
  RolAPI.obtener = obtener;
  RolAPI.listar = listar;
  RolAPI.guardar = guardar;
  RolAPI.eliminar = eliminar;
  return RolAPI;

  function obtener( id ) {
    if ( id ) {
      return $http.get( backend + "/api/rol/" + id ).then( function( resp ) {
        return rol( resp.data );
      } );
    } else {
      return rol( {} );
    }
  }

  function listar( pagina, cantidad ) {
    var params = {
      params: {
        pagina: pagina || 0,
        cantidad: cantidad || 10
      }
    };
    return $http.get( backend + "/api/rol/", params ).then( ok, err );
  }

  function ok( resp ) {
    if ( resp.data.docs ) {
      resp.data.docs = _.map( resp.data.docs, function( elem ) {
        return rol( elem );
      } );
    }
    return resp.data;
  }

  function err( ) {
    return {docs: [], contador: 0};
  }

  function eliminar( id ) {
    return $http.delete( backend + "/api/rol/" + id ).then( function( resp ) {
      return resp.data;
    }, function( resp ) {
      return resp;
    } );
  }

  function guardar( obj ) {
    if ( obj._id ) {
      return editar( obj );
    } else {
      return crear( obj );
    }
  }

  function crear( obj ) {
    return $http.post( backend + "/api/rol/", obj ).then( function( resp ) {
      return rol( resp.data );
    }, function( resp ) {
      return resp;
    } );
  }

  function editar( obj ) {
    return $http.put( backend + "/api/rol/" + obj._id, obj ).then( function( resp ) {
      return rol( resp.data );
    }, function( resp ) {
      return resp;
    } );
  }
}
