"use strict";

module.exports = TiposcajasAPI;
var _ = require( "lodash" );

var tiposcajas = require( "../../models/tiposcajas.js" );

function TiposcajasAPI( backend, $http ) {
  TiposcajasAPI.obtener = obtener;
  TiposcajasAPI.listar = listar;
  TiposcajasAPI.guardar = guardar;
  TiposcajasAPI.eliminar = eliminar;
  return TiposcajasAPI;

  function obtener( id ) {
    if ( id ) {
      return $http.get( backend + "/api/tiposcaja/" + id ).then( function( resp ) {
        return tiposcajas( resp.data );
      } );
    } else {
      return tiposcajas( {} );
    }
  }

  function listar( pagina, cantidad ) {
    var params = {
      params: {
        pagina: pagina || 0,
        cantidad: cantidad || 10
      }
    };
    return $http.get( backend + "/api/tiposcaja/", params ).then( ok, err );
  }

  function ok( resp ) {
    if ( resp.data.docs ) {
      resp.data.docs = _.map( resp.data.docs, function( elem ) {
        return tiposcajas( elem );
      } );
    }
    return resp.data;
  }

  function err( ) {
    return {docs: [], contador: 0};
  }

  function eliminar( id ) {
    return $http.delete( backend + "/api/tiposcaja/" + id ).then( function( resp ) {
      return resp.data;
    }, function( resp ) {
      return resp;
    } );
  }

  function guardar( obj ) {
    if ( obj._id ) {
      return editar( obj );
    } else {
      return crear( obj );
    }
  }

  function crear( obj ) {
    return $http.post( backend + "/api/tiposcaja/", obj ).then( function( resp ) {
      return tiposcajas( resp.data );
    }, function( resp ) {
      return resp;
    } );
  }

  function editar( obj ) {
    return $http.put( backend + "/api/tiposcaja/" + obj._id, obj ).then( function( resp ) {
      return tiposcajas( resp.data );
    }, function( resp ) {
      return resp;
    } );
  }
}
