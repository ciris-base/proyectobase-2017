"use strict";

var _ = require( "lodash" );

module.exports = {
  setearVer: setearVer,
  validarVer: validarVer
};

function setearVer( permisos, accion ) {
  if ( permisos[accion] ) {
    permisos.ver = true;
  }
}

function validarVer( permisos ) {
  var activos = _.filter( permisos, function( v, k ) {
    return k !== "ver" && permisos[k] === true;
  } );
  if ( activos.length ) {
    permisos.ver = true;
  }
}
