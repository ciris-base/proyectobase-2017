"use strict";

var _ = require( "lodash" );

module.exports = Tiposcajas;

var modelo = {
  hacerAlgo: hacerAlgo
};

function Tiposcajas( json ) {
  var temp = Object.create( modelo );
  var obj = _.assign( temp, json );
  return obj;
}

function hacerAlgo() {
  return "Algo";
}
