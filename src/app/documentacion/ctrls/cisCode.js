"use strict";

var hljs = require( "highlight.js" );

module.exports = cisCode;

function cisCode() {
  return {
    restrict: "E",
    link: link
  };
}

function link( $scope, $elem ) {
  hljs.configure( {
    languages: [ "javascript", "html" ]
  } );
  hljs.highlightBlock( $elem[0] );
}
