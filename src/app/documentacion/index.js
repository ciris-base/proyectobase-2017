"use strict";

var angular = require( "angular" );
var packageJson = require( "../../../package.json" );

var mod = angular.module( packageJson.name + ".documentacion", [] );
mod.config( require( "./rutas" ) );

//Controladores
mod.controller( "DocNotificacionesCtrl", require( "./ctrls/docNotificacionesCtrl.js" ) );
mod.controller( "DocModalsCtrl", require( "./ctrls/docModalsCtrl.js" ) );
mod.controller( "ModalPruebaCtrl", require( "./ctrls/modals/modalPruebaCtrl.js" ) );
mod.controller( "PanelCtrl", require( "./ctrls/panelCtrl.js" ) );

//Directivas
mod.directive( "pre", require( "./ctrls/cisCode.js" ) );

module.exports = mod.name;
