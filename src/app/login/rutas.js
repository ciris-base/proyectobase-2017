"use strict";

module.exports = rutas;

function rutas( $stateProvider ) {

  $stateProvider.state( "login", {
    templateUrl: "login/views/login.html",
    url: "/",
    controller: "LoginCtrl",
    controllerAs: "vm",
    free: true,
    data: {
      oculto: true,
      titulo: "Login",
      icono: "fa-home"
    }
  } );

}
