"use strict";

var modeloUsuario = require( "../models/usuario.js" );
var $ = require( "jquery" );

module.exports = LoginCtrl;

function LoginCtrl( $auth, toastr, $state, $window, odooHost, odooDB ) {
  var vm = this;
  vm.ingresar = ingresar;
  vm.usuario = {
    recordar: true
  };

  function ingresar( form, usuario ) {
    usuario.login.toLowerCase();
    form.$submitted = true;
    if ( form.$valid ) {
      $auth.login( usuario ).then( ok, alerta );
    } else {
      return toastr.warning( "Faltan campos por completar", "Advertencia" );
    }
  }

  function ok( res ) {
    var data = {
      "jsonrpc":"2.0",
      "method":"call",
      "r": JSON.stringify( {
        "params": {
          "db": odooDB,
          "login": vm.usuario.login,
          "password": vm.usuario.password
        }
      } )
    };
    var url = "http://" + odooHost + "/web/session/authenticate";
    $.ajax( {
      url: url,
      dataType: "jsonp",
      jsonp: "jsonp",
      type: "GET",
      cache: false,
      data: data
    } ).then( function( odooRes ) {
      res.data.usuario.sesionOdoo = odooRes.session_id;
      var usuario = modeloUsuario( res.data.usuario );
      $window.sessionStorage.usuario = JSON.stringify( usuario );
      if ( vm.usuario.recordar === true ) {
        $window.localStorage.usuario = JSON.stringify( usuario );
      }
      toastr.info( "Bienvenido " + usuario.nombre, "Información" );
      $state.go( "index.main" );
    } );
  }

  function alerta( resp ) {
    if ( resp.status === -1 ) {
      toastr.error( "Sin conexión con el servidor", "Error" );
    }
    toastr.warning( "Credenciales Inválidos", "Advertencia" );
  }
}
