# Proyecto Base Frontend


## Pasos de instalación
### 0) Instalar nvm

```bash
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.31.2/install.sh | bash
source ~/.bashrc
```

### 0-1) Instalar Node

```bash
nvm install 6.9.1
nvm use 6.9.1
```

### 0-2) Instalar los ejecutables globales

```bash
npm install -g gulp
```

### 1) Copiar el repositorio (No hace falta que sea un fork/clon)

```bash
https://gitlab.com/ciris/base-frontend
```


### 2) Instalar las dependencias del Proyecto

```bash
cd $proyecto
npm install
```

## Iniciarlizar el proyecto

```bash
npm run init
```

## Correr el proyecto
### Desarrollo Web
```bash
gulp
```

### Herramientas de QA
```bash
gulp qa
gulp test
```

# Documentación y Librerías
 * Reglex-grid    → http://leejordan.github.io/reflex/docs/
 * Angular        → https://angularjs.org/  |  Egghead: https://egghead.io/technologies/angularjs
 * Moment         → http://momentjs.com/docs
 * Lodash         → https://lodash.com/docs
 * Sass           → http://sass-lang.com/guide/
 * UI-Router      → https://github.com/angular-ui/ui-router/wiki
 * Satellizer     → https://github.com/sahat/satellizer
