"use strict";
var gulp = require( "gulp" );
var gutil = require( "gulp-util" );
var gulpif = require( "gulp-if" );
var sass = require( "gulp-sass" );
var rename = require( "gulp-rename" );
var sourcemaps = require( "gulp-sourcemaps" );
var browserSync = require( "./util/browserSyncInstance.js" );
var cleanCSS = require( "gulp-clean-css" );
var autoprefixer = require( "gulp-autoprefixer" );
var rutas = require( "./util/rutas.js" );
var variables = require( "./util/variablesEntorno.js" );
var notificar = require( "./util/notificar.js" );

module.exports = {
  main: main,
  printer: printer
};

function main( ) {
  return compilarSass( rutas.sass.main, "estilos.css" );
}

function printer( ) {
  return compilarSass( rutas.sass.printer, "estilos-print.css" );
}

function compilarSass( ruta, nombre ) {
  return gulp.src( [ ruta ] )
    .pipe( gulpif( !variables.produccion, sourcemaps.init() ) )
    .pipe( sass().on( "error", error ) )
    .pipe( autoprefixer( {
            browsers: [ "last 2 versions" ]
        } ) )
    .pipe( gulpif( variables.produccion, cleanCSS()  ) )
    .pipe( gulpif( !variables.produccion, sourcemaps.write( ) ) )
    .pipe( rename( nombre ) )
    .pipe( gulp.dest( rutas.dist.css ) )
    .pipe( gulpif( !variables.produccion, browserSync().stream() ) )
    .on( "error", notificar( "Error de Sass" ) );
}

function error( err ) {
  notificar( "Error de Sass" )( err );
  gutil.log( err.formatted );
  if ( !variables.produccion ) {
    this.emit( "end" );
  }
}
