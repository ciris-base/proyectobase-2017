"use strict";
var rimraf = require( "rimraf" );
var rutas = require( "./util/rutas.js" );

module.exports = limpiar;

function limpiar( callback ) {
  rimraf.sync( rutas.dist.base );
  return callback();
}
