"use strict";

var browserSync = require( "browser-sync" );

module.exports = getInstancia;
var instancia = null;
var nombre = "Ciris";

function getInstancia() {
  if ( !browserSync.has( nombre ) ) {
    instancia = browserSync.create( nombre );
  }
  return instancia;
}
