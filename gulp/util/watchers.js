"use strict";

var gulp = require( "gulp" );
var _ = require( "lodash" );
var rutas = require( "./rutas.js" );

module.exports = watch;

function watch() {
  gulp.watch( rutas.sass.todo, [ "build:sass", "build:sass-printer" ] );
  gulp.watch( rutas.plantillas, [ "build:html" ] );
  gulp.watch( rutas.recursos, [ "build:recursos" ] );
  gulp.watch( rutas.htmlInicial, [ "build:html-inicial" ] );
}
