"use strict";

module.exports = {
  produccion: ( process.env.NODE_ENV ? process.env.NODE_ENV === "production" : false ),
  backend: process.env.BACKEND || "http://localhost:3001",
  daemon: process.env.daemon || "http://localhost:3002",
  odooHost: process.env.ODOO_HOST || "odoo.ciriscr.com",
  odooDB: process.env.ODOO_DB || "ciris"
};
