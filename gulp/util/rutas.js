"use strict";
var config = {tipo: "Web"};

var dist = {
  "Web": {
    "base": "www",
    "js": "www",
    "css": "www"
  },
  "Ionic": {
    "base": "www",
    "js": "www/js",
    "css": "www/css"
  },
  "Electron": {
    "base": "app",
    "js": "app",
    "css": "app"
  }
};

var rutas = {
  "dist": dist[config.tipo],
  "scripts": {
    "main": "src/app/index.js",
    "todo": [ "src/app/*.js", "src/app/**/*.js" ],
    "istanbul": [ "src/app/*.js", "src/app/**/*.js", "!src/app/**/*/test/unit/**/*.js" ]
  },
  "tests": {
    "unit": "src/app/**/*/test/unit/**/*.js"
  },
  "sass": {
    "main": "src/sass/main.scss",
    "printer": "src/sass/printer.scss",
    "todo": [ "src/sass/*.scss", "src/sass/**/*.scss", "src/app/**/*.scss" ]
  },
  "recursos": [ "src/recursos", "src/recursos/*", "src/recursos/**/*" ],
  "htmlInicial": "src/app/index.html",
  "plantillas": [ "src/app/**/*.html", "!src/app/index.html" ]
};

module.exports = rutas;
