"use strict";
var gulp = require( "gulp" );
var rutas = require( "./util/rutas.js" );
var notificar = require( "./util/notificar.js" );

module.exports = htmlInicial;

function htmlInicial() {
  return gulp.src( rutas.htmlInicial, {
      base: "./src/app"
    } )
    .pipe( gulp.dest( rutas.dist.base ) )
    .on( "error", notificar( "Error copiando los recursos" ) );
}
