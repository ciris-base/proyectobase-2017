"use strict";

var gulp = require( "gulp" );
var tape = require( "gulp-tape" );
var faucet = require( "faucet" );
var istanbul = require( "gulp-istanbul" );
var rutas = require( "./util/rutas.js" );

module.exports = {
  test: test,
  coverage: coverage
};

var thresholds = {
  thresholds: {
    global: 70
  },
  emits: true
};

function coverage() {
  return gulp.src( rutas.scripts.istanbul )
    .pipe( istanbul( {includeUntested: false} ) )
    .pipe( istanbul.hookRequire() );
}

function test() {
  return gulp.src( rutas.tests.unit )
    .pipe( tape( {
      bail: false,
      reporter: faucet()
    } ) )
    .pipe( istanbul.writeReports() )
    .pipe( istanbul.enforceThresholds( thresholds ) );
}
