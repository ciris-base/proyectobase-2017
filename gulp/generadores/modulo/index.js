"use strict";

var angular = require( "angular" );
var packageJson = require( "../../../package.json" );

var mod = angular.module( packageJson.name + ".NOMBREMODULO", [] );
mod.config( require( "./rutas" ) );

mod.controller( "NOMBREMODULOCtrl", require( "./ctrls/ARCHIVOMODULOCtrl.js" ) );

module.exports = mod.name;
