"use strict";

module.exports = rutas;

function rutas( $stateProvider ) {

  $stateProvider.state( "index.ARCHIVOMODULO", {
    templateUrl: "ARCHIVOMODULO/views/inicio.html",
    url: "/ARCHIVOMODULO",
    controller: "NOMBREMODULOCtrl",
    controllerAs: "mod",
    data: {
      titulo: "Módulo NOMBREMODULO",
      icono: "fa-home"
    }
  } );

}
